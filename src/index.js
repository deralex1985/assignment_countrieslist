import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import CountriesApp from './CountriesApp'

ReactDOM.render(<CountriesApp/>, document.getElementById('root'));
