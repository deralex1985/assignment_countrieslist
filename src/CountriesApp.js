import React, { Component } from 'react'
import ShowCountries from './ShowCountries';

export default class CountriesApp extends Component {

    readCountries=[]
    tableHeader=[]
      
    render() {

        return (
            <div className="container">
                <h1>Länder in Landessprache</h1>
                <p>
                    Quelle: <a href="https://restcountries.eu/rest/v2/all">https://restcountries.eu/rest/v2/all</a>
                </p>
                <button
                onClick={this.loadCountries} >Jetzt Länder Laden</button>
                <br/>   
                
                <ShowCountries  
                  countries={this.readCountries}
                  tableHeader={this.tableHeader}
                  dummy={"TestTextAlexDummy"} />
        
            </div>
        )
    }


    loadCountries=(event,readCountries,tableHeader)=>
    { 
        readCountries=this.readCountries
        tableHeader=this.tableHeader  
        console.log("button clicked al")
        fetch('https://restcountries.eu/rest/v2/all')
            .then((res) => res.json())
            .then(payload => 
                    {
                        payload.map(country => {
                                let namePair={
                                        german: country.name,
                                        native: country.nativeName
                                             }             
                                readCountries.push(namePair)             
                                this.setState({countries: readCountries})
                                return readCountries
                        })
                        
                        this.setState({countries: readCountries})
                        
                    })
            .catch(err => console.log("Error:", err));

            let header={
                column1: "English Name",
                column2: "Native Name"}
            tableHeader.push(header)
            this.setState({tableHeader: tableHeader})
    } 
    

   

}

