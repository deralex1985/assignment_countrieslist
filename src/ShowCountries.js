import React, { Component } from 'react';

const TableHeader= props => { 
  const rows = props.tableHeader.map((row, index) => {
      return (
          <tr key={index}>
              <td><strong>{row.column1}</strong></td>
              <td><strong>{row.column2}</strong></td>
          </tr>
      );
  });

  return <tbody>{rows}</tbody>;
}


const TableBody = props => { 
    const rows = props.countries.map((row, index) => {
        return (
            <tr key={index}>
                <td>{row.german}</td>
                <td>{row.native}</td>
            </tr>
        );
    });

    return <tbody>{rows}</tbody>;
}


class ShowCountries extends Component {
    render() {
        const {countries} = this.props
        const {tableHeader} = this.props
    
        return (
          <table>
            <TableHeader tableHeader={tableHeader}/>
            <TableBody countries={countries} />
          </table>
        )
      }
    }

export default ShowCountries